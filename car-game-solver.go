package main

import (
   "fmt"
   "github.com/fatih/color"
   "strconv"
   //"errors"
)


type Axis string

type Vehicle struct {
   Id int
   Orientation Axis
   Boundary_Row_or_Column int
   Position []int
   Type string
}


// Index returns the first index of the target string t, or -1 if no match is found.
func Index_string(vs []string, t string) int {
   for i, v := range vs {
      if v == t {
         return i
      }
   }
   return -1
}

// Include returns true if the target string t is in the slice.
func Include_string(vs []string, t string) bool {
   return Index_string(vs, t) >= 0
}

// Index returns the first index of the target int t, or -1 if no match is found.
func Index_int(vi []int, t int) int {
   for i, v := range vi {
      if v == t {
         return i
      }
   }
   return -1
}

// Include returns true if the target int t is in the slice.
func Include_int(vi []int, t int) bool {
   return Index_int(vi, t) >= 0
}

func Array_Column(array [6][6]int, columnIndex int) (column [6]int) {
   for i, row := range array {
      column[i] = row[columnIndex]
   }
   return column
}

func IntSlice_MinMax(s []int) (min int, max int) {
   if len(s) > 0 {
      min = s[0]
      max = s[0]
      for _, value := range s {
         if value < min {
            min = value
         }
         if value > max {
            max = value
         }
      }
      return min, max
   } else {
      return 0, 0
   }
}

/*
 * Enumerate all the different numbers (car ids) in the 6x6 matrix
 */
func Enumerate(M [6][6]int) (car_id_list []int) {
   for i := 0; i < 6; i++ {
      for j := 0; j < 6; j++ {
         //If the selected coordinate contains a car id
         if M[i][j] != 0 {
            //If the selected car id isn't already in the list
            in_list := false
            for _, value := range car_id_list {
               if M[i][j] == value {
                  in_list = true
               }
            } //end of list check
            if in_list == false {
               car_id_list = append(car_id_list, M[i][j])
            }
         }
      } //end of column for loop
   } //end of row for loop
   return car_id_list
}

// Enumerates all the cars present in a 1D slice (part of a row or a column)
func Enumerate_from_Slice(S []int) (car_id_list []int) {
   for _, val := range S {
      if val != 0 {
         in_list := false
         for _, car_id := range car_id_list {
            if val == car_id {
               in_list = true
            }
         } //end of list check
         if in_list == false {
            car_id_list = append(car_id_list, val)
         }
      }
   } //end of slice range loop
   return car_id_list
}

// Reverse the order of a slice
func Reverse_int_Slice(S []int) (reversed_slice []int) {
   reversed_slice = S
   for i, j := 0, len(S)-1; i < j; i, j = i+1, j-1 {
      reversed_slice[i], reversed_slice[j] = reversed_slice[j], reversed_slice[i]
   }
   return reversed_slice
}

/*
 * Updates the 6x6 matriix given a list of vehicles
 */
func Update_Map(car_data map[int]Vehicle) (M [6][6]int) {
   for _, v := range car_data {
      for _, pos := range v.Position {
         if v.Orientation == "UD" {
            M[pos][v.Boundary_Row_or_Column] = v.Id
         } else if v.Orientation == "LR" {
            M[v.Boundary_Row_or_Column][pos] = v.Id
         }
      }
   }
   return M
}

/*
 *  Let's suppose that a car is at least 2 cases long, on a grid of 36 cases (6x6)
 *  we can expect that the number of cars on the grid will never be over 18 (36/2)
 *  hence, let's define 18 colors + a specific color for the target car (the "Yellow" one)
 */
func Car_Num_and_Color(car_id int) string {
   //One specific color for each possible car nuumber between 1 and 18
   car_1 := color.New(color.FgWhite, color.BgCyan).SprintFunc()
   car_2 := color.New(color.FgWhite, color.BgRed).SprintFunc()
   car_3 := color.New(color.FgBlue, color.BgGreen).SprintFunc()
   car_4 := color.New(color.FgHiMagenta).SprintFunc()
   car_5 := color.New(color.FgYellow, color.BgMagenta).SprintFunc()
   car_6 := color.New(color.FgRed, color.BgCyan).SprintFunc()
   car_7 := color.New(color.FgYellow, color.BgRed).SprintFunc()
   car_8 := color.New(color.FgMagenta, color.BgGreen).SprintFunc()
   car_9 := color.New(color.FgMagenta, color.BgBlue).SprintFunc()
   car_10 := color.New(color.FgHiCyan, color.BgWhite).SprintFunc()
   car_11 := color.New(color.FgHiCyan).SprintFunc()
   car_12 := color.New(color.FgHiMagenta, color.BgWhite).SprintFunc()
   car_13 := color.New(color.FgWhite, color.BgBlue).SprintFunc()
   car_14 := color.New(color.FgHiYellow).SprintFunc()
   car_15 := color.New(color.FgRed, color.BgWhite).SprintFunc()
   car_16 := color.New(color.FgHiRed).SprintFunc()
   car_17 := color.New(color.FgHiGreen, color.BgWhite).SprintFunc()
   car_18 := color.New(color.FgWhite).SprintFunc()
   //A specific color for the "Yellow car"
   yellow_car := color.New(color.FgHiRed, color.BgHiYellow).SprintFunc()
   //...
   //Now let's associate each formating with a number
   return_string := ""
   if car_id < 10 {
      return_string = fmt.Sprintf(" %01d", car_id)
   } else {
      return_string = fmt.Sprintf("%02d", car_id)
   }
   switch car_id {
   case 1:
      return_string = car_1(return_string)
      break
   case 2:
      return_string = car_2(return_string)
      break
   case 3:
      return_string = car_3(return_string)
      break
   case 4:
      return_string = car_4(return_string)
      break
   case 5:
      return_string = car_5(return_string)
      break
   case 6:
      return_string = car_6(return_string)
      break
   case 7:
      return_string = car_7(return_string)
      break
   case 8:
      return_string = car_8(return_string)
      break
   case 9:
      return_string = car_9(return_string)
      break
   case 10:
      return_string = car_10(return_string)
      break
   case 11:
      return_string = car_11(return_string)
      break
   case 12:
      return_string = car_12(return_string)
      break
   case 13:
      return_string = car_13(return_string)
      break
   case 14:
      return_string = car_14(return_string)
      break
   case 15:
      return_string = car_15(return_string)
      break
   case 16:
      return_string = car_16(return_string)
      break
   case 17:
      return_string = car_17(return_string)
      break
   case 18:
      return_string = car_18(return_string)
      break
   case 99:
      return_string = yellow_car(return_string)
      break
   default:
      return_string = car_18(return_string)
      break
   }
   return return_string
}

//Input the content of a 6x6 matrix from stdin (CLI) using "fmt"
func Full_Input(M *[6][6]int) (car_data map[int]Vehicle) {
   //yellow := color.New(color.FgYellow).SprintFunc()
   red := color.New(color.FgRed).SprintFunc()
   cyan := color.New(color.FgCyan).SprintFunc()
   coordinates := [6]string{"A", "B", "C", "D", "E", "F"}
   car_data = map[int]Vehicle{}
   //...
	for i := 0; i < 6; i++ {
		for j := 0; j < 6; j++ {
         fmt.Println("-----------------------------------------------------")
         fmt.Println("      1    2    3    4    5    6   ")
			fmt.Println("  =================================")
			for k := 0; k < 6; k++ {
            //Rows currentlt being edited
            //Formated only until the current position of the cursor (column)
            fmt.Printf("%s || ", coordinates[k])
            if k == i {
               //Columns before the cursor
               for l := 0; l < j; l++ {
                  if M[k][l] > 0 {
                     fmt.Printf("%s", Car_Num_and_Color(M[k][l]))
                  } else {
                     fmt.Printf("  ")
                  }
                  fmt.Printf(" | ")
               }
               //Cursor (column)
               fmt.Printf("%s", red("??"))
               //Columns after the cursor
               for l := j+1; l < 6; l++ {
                  fmt.Printf(" | ")
                  fmt.Printf("%02d", M[k][l])
               }
               if k == 2 {
                  fmt.Printf(" %s \n", red(">>>"))
               } else {
                  fmt.Printf(" || \n")
               }
               if k < 5 {
                  fmt.Printf("  ---------------------------------\n")
               } else {
                  fmt.Printf("  =================================\n")
               }
            //Rows not currently edited
            //Only the rows that have been edited should have a special formating
				} else {
               //Rows that has been edited already
               if k < i {
                  for l := 0; l < 6; l++ {
                     if M[k][l] > 0 {
                        fmt.Printf("%s", Car_Num_and_Color(M[k][l]))
                     } else {
                        fmt.Printf("  ")
                     }
                     if l < 5 {
                        fmt.Printf(" | ")
                     }
                  }
                  if k == 2 {
                     fmt.Printf(" %s \n", red(">>>"))
                  } else {
                     fmt.Printf(" || \n")
                  }
                  if k < 5 {
                     fmt.Printf("  ---------------------------------\n")
                  } else {
                     fmt.Printf("  =================================\n")
                  }
               //Rows that are still to be edited
               } else {
                  fmt.Printf("%02d | %02d | %02d | %02d | %02d | %02d", M[k][0], M[k][1], M[k][2], M[k][3], M[k][4], M[k][5])
                  if k == 2 {
                     fmt.Printf(" %s \n", red(">>>"))
                  } else {
                     fmt.Printf(" || \n")
                  }
                  if k < 5 {
                     fmt.Printf("  ---------------------------------\n")
                  } else {
                     fmt.Printf("  =================================\n")
                  }
               }
				}
			}
         // Prompt
			fmt.Print("\n")
         fmt.Println(cyan("Please identify each car with a different number."))
         fmt.Println(cyan("The Yellow Car should be identified with number 99."))
         fmt.Println(cyan("A space with no car should be 0 or left blank."))
			fmt.Print("Enter an integer value for the selected case: ")
			_, err := fmt.Scanf("%d", &M[i][j])
			if err != nil {
            if err.Error() == "unexpected newline" {
               fmt.Println("no car here.")
            } else {
				   fmt.Println(err)
            }
			}
			//fmt.Printf("You have entered : %f \n\n", M[i][j])
			fmt.Print("\n\n")
		}
	} //end of main for loop
   //Now let's analyze the position of the cars (and trucks)
   //form the previously filled array (M)
   car_list := Enumerate(*M)
   for _, car_i := range car_list {
      //find all coordinates for each vehicle, and retrieve the corresponding data
      var c_position []int
      var r_position []int
      var v_type string
      for i := 0; i < 6; i++ {
         for j := 0; j < 6; j++ {
            if M[i][j] == car_i {
               r_position = append(r_position, i)
               c_position = append(c_position, j)
            }
         } //end of columns loop
      } //end of rows loop
      if r_position[0] == r_position[1] {
         // The vehicle is in the "Left-Right" direction and is bound to a row
         if len(c_position) == 2 {
            v_type = "car"
         } else if len(c_position) == 3 {
            v_type = "truck"
         }
         car_data[car_i] = Vehicle{Id: car_i, Orientation: "LR", Boundary_Row_or_Column: r_position[0], Position: c_position, Type: v_type}
      } else if c_position[0] == c_position[1] {
         // The vehicle is in the "Up-Down" direction and is bound to a column
         if len(r_position) == 2 {
            v_type = "car"
         } else if len(r_position) == 3 {
            v_type = "truck"
         }
         car_data[car_i] = Vehicle{Id: car_i, Orientation: "UD", Boundary_Row_or_Column: c_position[0], Position: r_position, Type: v_type}
      }
   } // end of car_list loop
   return car_data
}

//Display uses "fmt" to print a 6x6 matrix with simple formating
func Display(M [6][6]int) {
   red := color.New(color.FgRed).SprintFunc()
   coordinates := [6]string{"A", "B", "C", "D", "E", "F"}
   fmt.Println("-----------------------------------------------------")
   fmt.Println("      1    2    3    4    5    6   ")
   fmt.Println("  =================================")
	for i := 0; i < 6; i++ {
      fmt.Printf("%s || ", coordinates[i])
      for j := 0; j < 6; j++ {
         if M[i][j] > 0 {
            fmt.Printf("%s", Car_Num_and_Color(M[i][j]))
         } else {
            fmt.Printf("  ")
         }
         if j < 5 {
            fmt.Printf(" | ")
            //fmt.Printf(" ")
         }
      }
      if i == 2 {
         fmt.Printf(" %s \n", red(">>>"))
      } else {
         fmt.Printf(" || \n")
      }
      if i < 5 {
         fmt.Printf("  ---------------------------------\n")
      } else {
         fmt.Printf("  =================================\n")
      }
	}
	fmt.Print("\n")
}

//Alternative input method in which each car is entered one by one by coordinates
func Selective_Input(M *[6][6]int) (car_data map[int]Vehicle) {
   cyan := color.New(color.FgCyan).SprintFunc()
   coordinates := [6]string{"A", "B", "C", "D", "E", "F"}
   var number_of_cars int
   car_data = map[int]Vehicle{}

   fmt.Printf(cyan("Including the yellow car, how many vehicles are there on the map?\n"))
   fmt.Printf("Number of vehicles: ")
   _, err := fmt.Scanf("%d", &number_of_cars)
   if err != nil {
      fmt.Println(err)
   }
   if number_of_cars <= 0 {
      fmt.Println("The map will be left empty...\n")
   } else {
      for car_i := 0; car_i < number_of_cars; car_i++ {
         var position []int
         var orientation Axis
         var row_num, col_num int
         fmt.Printf("\n")
         Display(*M)
         if car_i == 0 {
            fmt.Printf("Yellow car\n-----------------------------------------------------\n")
            fmt.Printf("Vehicle length fixed to '2'.\n")
            fmt.Printf("The row is fixed to '%s'.\n", coordinates[2])
            fmt.Printf("Space separated column numbers (x x): ")
            var d1, d2 int
            _, err := fmt.Scanf("%d %d", &d1, &d2)
            if err != nil {
               fmt.Println(err)
            }
            position = append(position, d1 - 1, d2 - 1)
            car_data[99] = Vehicle{Id: 99, Orientation: "LR", Boundary_Row_or_Column: 2, Position: position, Type: "car"}
            *M = Update_Map(car_data)
         } else {
            fmt.Printf("Vehicle %d\n-----------------------------------------------------\n", car_i)
            //Ask for the vehicle length
            fmt.Printf("Vehicle length (2|3): ")
            var length int
            _, err1 := fmt.Scanf("%d", &length)
            if err1 != nil {
               fmt.Println(err1)
            }
            //Ask for the row name or column number
            fmt.Printf("Row or Column name: ")
            var Row_or_Column string
            _, err2 := fmt.Scanf("%s", &Row_or_Column)
            if err2 != nil {
               fmt.Println(err2)
            }
            // if the scanned string is in the coordinates list, it is a letter (row)
            if Include_string(coordinates[:], Row_or_Column) {
               row_num = Index_string(coordinates[:], Row_or_Column)
               orientation = "LR"
            } else { // otherwise, it should be a number (column)
               cn, err3 := strconv.Atoi(Row_or_Column)
               col_num = cn - 1 // because column "1" is index "0" in the matrix
               if err3 != nil {
                  fmt.Println(err3)
               }
               orientation = "UD"
            }
            if length == 2 {
               if orientation == "LR" {
                  fmt.Printf("Space separated column numbers (x x): ")
                  var d1, d2 int
                  _, err := fmt.Scanf("%d %d", &d1, &d2)
                  if err != nil {
                     fmt.Println(err)
                  }
                  position = append(position, d1 - 1, d2 - 1)
                  car_data[car_i] = Vehicle{Id: car_i, Orientation: orientation, Boundary_Row_or_Column: row_num, Position: position, Type: "car"}
               } else if orientation == "UD" {
                  fmt.Printf("Space separated row names (X X): ")
                  var s1, s2 string
                  _, err := fmt.Scanf("%s %s", &s1, &s2)
                  if err != nil {
                     fmt.Println(err)
                  }
                  position = append(position, Index_string(coordinates[:], s1), Index_string(coordinates[:], s2))
                  car_data[car_i] = Vehicle{Id: car_i, Orientation: orientation, Boundary_Row_or_Column: col_num, Position: position, Type: "car"}
               }
            } else if length == 3 {
               if orientation == "LR" {
                  fmt.Printf("Space separated column numbers (x x x): ")
                  var d1, d2, d3 int
                  _, err := fmt.Scanf("%d %d %d", &d1, &d2, &d3)
                  if err != nil {
                     fmt.Println(err)
                  }
                  position = append(position, d1 - 1, d2 - 1, d3 - 1)
                  car_data[car_i] = Vehicle{Id: car_i, Orientation: orientation, Boundary_Row_or_Column: row_num, Position: position, Type: "truck"}
               } else if orientation == "UD" {
                  fmt.Printf("Space separated row names (X X X): ")
                  var s1, s2, s3 string
                  _, err := fmt.Scanf("%s %s %s", &s1, &s2, &s3)
                  if err != nil {
                     fmt.Println(err)
                  }
                  position = append(position, Index_string(coordinates[:], s1), Index_string(coordinates[:], s2), Index_string(coordinates[:], s3))
                  car_data[car_i] = Vehicle{Id: car_i, Orientation: orientation, Boundary_Row_or_Column: col_num, Position: position, Type: "truck"}
               }
            }
            *M = Update_Map(car_data)
         }
      } //end of car input loop
   }
   fmt.Printf("\n\n")
   return car_data
}

//Move a vehicle on the map
func (v Vehicle) move(u int) {
   for i, _ := range v.Position {
      v.Position[i] += u
   }
}

//Return a list of possible moves for the vehicle
func (v Vehicle) possible_moves(M [6][6]int) (possible_moves_list []int) {
   /* Method :
      1) get the boundary row or column of the vehicle
      2) get the minimum and maximum of the position slice
      3) in the boundary ow or column, try to move below the minimum position
         and beyond the maximum position
      4) if the value encountered is '0', count 1 move and continue; stop otherwise
   */
   var row_or_column [6]int
   if v.Orientation == "LR" {
      row_or_column = M[v.Boundary_Row_or_Column]
   } else if v.Orientation == "UD" {
      row_or_column = Array_Column(M, v.Boundary_Row_or_Column)
   }
   //fmt.Println("row or column:", row_or_column)
   pos_min, pos_max := IntSlice_MinMax(v.Position)
   for pos := pos_min - 1; pos >= 0; pos-- {
      if row_or_column[pos] == 0 {
         possible_moves_list = append(possible_moves_list, pos - pos_min)
      } else {
         break
      }
   }
   for pos := pos_max + 1; pos < 6; pos++ {
      if row_or_column[pos] == 0 {
         possible_moves_list = append(possible_moves_list, pos - pos_max)
      } else {
         break
      }
   }
   return possible_moves_list
}

//Move a vehicle on the map if possible
func (v Vehicle) check_and_move(u int, M [6][6]int) (ok bool) {
   if Include_int(v.possible_moves(M), u) {
      v.move(u)
      ok = true
   } else {
      if u == 0 {
         ok = true
      } else {
         fmt.Printf("The %s number %d can't perform a move of value %d\n\n", v.Type, v.Id, u)
         ok = false
      }
   }
   return ok
}

//Find the moves required for the car (car_id) to be able to move according to u
func Required_moves(car_id int, u int, M [6][6]int, car_data map[int]Vehicle) (moves_map map[int][]int, moves_order []int, o_car_data map[int]Vehicle) {
   yellow := color.New(color.FgYellow).SprintFunc()
   green := color.New(color.FgGreen).SprintFunc()
   moves_map = map[int][]int{}
   p_car_data := car_data
   p_M := M
   p_M = Update_Map(p_car_data)
   p_car_id := car_id
   p_moves_avancement := map[int]int{}
   p_reverting := false
   if Include_int(p_car_data[p_car_id].possible_moves(p_M), u) {
      fmt.Println(p_car_id, "can move of", u, "directly:", p_car_data[p_car_id].possible_moves(p_M))

   } else {
      var row_or_column [6]int
      var path []int
      var hindrance_list []int
      //Display(p_M)
      //fmt.Println(p_car_data)
      if p_car_data[p_car_id].Orientation == "LR" {
         row_or_column = p_M[p_car_data[p_car_id].Boundary_Row_or_Column]
      } else if p_car_data[p_car_id].Orientation == "UD" {
         row_or_column = Array_Column(p_M, p_car_data[p_car_id].Boundary_Row_or_Column)
      }
      pos_min, pos_max := IntSlice_MinMax(p_car_data[p_car_id].Position)
      if u > 0 {
         // The vehicle needs to move right or down from its pos_max
         if pos_max + u < 6 {
            // The move is within the limits of the matrix and might be possible
            path = row_or_column[pos_max+1:pos_max+u+1]
            fmt.Println(p_car_id, "u:", u, "path:", path)
         } else {
            // The move goes beyond the limits of the matrix, and is thus impossible
            // Call the function again with a rectified move ?
            return Required_moves(p_car_id, u - (1 + pos_max + u - 6), p_M, p_car_data)
         }
      } else if u < 0 {
         // The vehicle needs to move left or up from its pos_min
         if pos_min + u >= 0 {
            // The move is within the limits of the matrix and might be possible
            path = row_or_column[pos_min+u:pos_min]
            //path = Reverse_int_Slice(path) //not needed if the hindrance_list is reversed later
            fmt.Println(p_car_id, "u:", u, "path:", path)
         } else {
            // The move goes beyond the limits of the matrix, and is thus impossible
            // Call the function again with a rectified move ?
            return Required_moves(p_car_id, -pos_min, p_M, p_car_data)
         }
      } else {
         // Case where, for some reason(??) u == 0
         moves_order = append(moves_order, p_car_id)
         moves_map[car_id] = append(moves_map[p_car_id], u)
      }
      // The following should be the same no matter the direction of the car and the value of u
      hindrance_list = Enumerate_from_Slice(path)
      // Supposing we have more than one hindrance
      // If the move is towards the left (u < 0), the list should be reversed to reflect the actual order
      if u < 0 {
         hindrance_list = Reverse_int_Slice(hindrance_list)
      }
      // Treating hindrance from the closest to the farthest
      for _, h := range hindrance_list {
         fmt.Printf("%d is hindering %d\n", h, p_car_id)
         var next_move int
         //Is the hindrance bound to the same row / column? Or can it move away?
         if p_car_data[p_car_id].Orientation != p_car_data[h].Orientation {
            // The two vehicles do not have to share the same row / column
            h_pos_min, h_pos_max := IntSlice_MinMax(p_car_data[h].Position)
            var candidates []int
            if p_car_data[p_car_id].Boundary_Row_or_Column == h_pos_min {
               // In order to get out of the way, the car needs to move of +1 or -Length
               if h_pos_max + 1 < 6 {
                  candidates = append(candidates, 1)
               }
               if h_pos_min - len(car_data[h].Position) >= 0 {
                  candidates = append(candidates, -len(p_car_data[h].Position))
               }
            } else if p_car_data[p_car_id].Boundary_Row_or_Column == h_pos_max {
               // In order to get out of the way, the car needs to move of -1 or +Length
               if h_pos_min - 1 >= 0 {
                  candidates = append(candidates, -1)
               }
               if h_pos_max + len(p_car_data[h].Position) < 6 {
                  candidates = append(candidates, len(p_car_data[h].Position))
               }
            } else {
               // In order to get out of the way, the car needs to move of -2 or +2
               if h_pos_min - 2 >= 0 {
                  candidates = append(candidates, -2)
               }
               if h_pos_max + 2 < 6 {
                  candidates = append(candidates, 2)
               }
            }
            // Now, a candidate should be chosen if there is more than one...
            if len(candidates) > 1 {
               //How to choose?
               //Check whether one of the moves is possible right away,
               //If both are, choose the first one (should be the smallest one)
               if Include_int(p_car_data[h].possible_moves(p_M), candidates[0]) {
                  next_move = candidates[0]
               } else if Include_int(p_car_data[h].possible_moves(p_M), candidates[1]) {
                  next_move = candidates[1]
               } else {
                  //If none are, check the amount of free space on both sides
                  var free_space_L, free_space_R int
                  free_space_L = 0
                  free_space_R = 0
                  for i := h_pos_min; i >= 0; i-- {
                     if p_car_data[h].Orientation == "LR" {
                        // We are on a row...
                        if p_M[p_car_data[h].Boundary_Row_or_Column][i] == 0 {
                           free_space_L++
                        }
                     } else if p_car_data[h].Orientation == "UD" {
                        // We are on a column...
                        if p_M[i][p_car_data[h].Boundary_Row_or_Column] == 0 {
                           free_space_L++
                        }
                     }
                  }
                  for i := h_pos_max; i < 6; i++ {
                     if p_car_data[h].Orientation == "LR" {
                        // We are on a row...
                        if p_M[car_data[h].Boundary_Row_or_Column][i] == 0 {
                           free_space_R++
                        }
                     } else if p_car_data[h].Orientation == "UD" {
                        // We are on a column...
                        if p_M[i][p_car_data[h].Boundary_Row_or_Column] == 0 {
                           free_space_R++
                        }
                     }
                  }
                  if free_space_L >= free_space_R {
                     // Choose the candidate that goes left or up
                     if candidates[0] < 0 {
                        next_move = candidates[0]
                     } else {
                        next_move = candidates[1]
                     }
                  } else {
                     // Choose the candidate that goes right or down
                     if candidates[0] < 0 {
                        next_move = candidates[1]
                     } else {
                        next_move = candidates[0]
                     }
                  }
               }
            } else {
               next_move = candidates[0]
            }
         } else {
            // The two vehicles ('car_id' and 'h') have to share the same row / column
            // The maximum displacement a vehicle can make is very limited
            // This means 'h' should be able to move enough to "free" the way for 'car_id'
            h_pos_min, h_pos_max := IntSlice_MinMax(p_car_data[h].Position)
            // If 'car_id' is trying to move Left or Up (u < 0), h_pos_max < pos_min
            // From this we may be able to calculate a distance between the two objects,
            // And deduce how much space shoould be freed
            // This is in the case the hindering car isn't against n edge of the map
            fmt.Println("h_pos_min:", h_pos_min, " , h_pos_max:", h_pos_max)
            if h_pos_max < 5 && h_pos_min > 0 {
               fmt.Println(h, "is not in an edge of the map")
               var h_distance int
               if u < 0 {
                  // We can suppose h_pos_max < pos_min
                  // If the two objects are adjacent, assume a distance of 0
                  h_distance = pos_min - h_pos_max - 1
                  // Deduce that 'h' should move of u +/- h_distance
                  // ex : -2 with space of 1 => -1
                  next_move = u + h_distance
                  //fmt.Println(u, next_move, h_distance)
               } else if u > 0 {
                  // We can suppose pos_max < h_pos_min
                  h_distance = h_pos_min - pos_max - 1
                  // Deduce that 'h' should move of u +/- h_distance
                  // ex : 2 with space of 1 => 1
                  next_move = u - h_distance
                  //fmt.Println(u, next_move, h_distance)
               }
            } else {
               // In the case the hindering car is against an edge of the map,
               // The move is impossible, so 'car_id' should move in the other direction
               fmt.Println(h, "is in an edge of the map and can't move...")
               p_reverting = true
               /*if len(moves_map[h]) > 0 {
                  moves_map[h] = moves_map[h][:len(moves_map[h])-1]
               }*/
               h = p_car_id
               switch u {
               case 1:
                  next_move = -len(p_car_data[p_car_id].Position)
                  break
               case -len(p_car_data[p_car_id].Position):
                  next_move = 1
                  break
               case len(p_car_data[p_car_id].Position):
                  next_move = -1
                  break
               case -1:
                  next_move = len(p_car_data[p_car_id].Position)
                  break
               case 2:
                  next_move = -2
                  break
               case -2:
                  next_move = 2
                  break
               default:
                  if u < 0 {
                     next_move = 2
                  } else {
                     next_move = -2
                  }
                  break
               }
            }
         }
         fmt.Scanf("%s")
         fmt.Println("p_reverting:", p_reverting)

         fmt.Println(yellow(fmt.Sprintf("Checking requirements for %d to move...", h)))
         //before adding this move, add its requirements
         req_map, req_order := moves_map, moves_order
         /*if p_reverting == false {
            req_map, req_order, p_car_data = Required_moves(h, next_move, p_M, p_car_data)
         } else {
            req_map, req_order, p_car_data = Required_moves(h, 0, p_M, p_car_data)
         }*/
         req_map, req_order, p_car_data = Required_moves(h, next_move, p_M, p_car_data)
         //moves_map[h] = append(moves_map[h], next_move)
         for k, v := range req_map {
            moves_map[k] = v
         }
         moves_order = req_order
      }

   } // end of "can't move directly" condition
   fmt.Println(yellow(fmt.Sprintf("Saving the list of required moves...")))
   if u != 0 {
      moves_map[p_car_id] = append(moves_map[p_car_id], u)
      moves_order = append(moves_order, p_car_id)
      if p_reverting == true {
         if len(moves_map[p_car_id]) > 0 {
            moves_map[p_car_id] = moves_map[p_car_id][:len(moves_map[p_car_id])-1]
         }
         if len(moves_order) > 0 {
            moves_order = moves_order[:len(moves_order)-1]
         }
      }
   }
   //fmt.Println(moves_map, moves_order)
   /* Now that the first list of constraints has been computed backwards,
      the feasibility of the whole process should be verified
      (because of each constraint being calculated without knowing the changes
      that are to be performed upstream...)
      Necessary adjustments should be inserted in the list at the proper time
   */
   //fmt.Println(yellow(fmt.Sprintf("Creating virtual map and car_data for forward check...")))

   //p_car_data := map[int]Vehicle{}
   // Note: a deep copy is needed to preserve the original values
   /*for k, _ := range car_data {
      new_car := car_data[k]
      var new_position []int
      for _, v := range car_data[k].Position {
         new_position = append(new_position, v)
      }
      new_car.Position = new_position
      p_car_data[k] = new_car
   }*/
   //experimental
   var p_move int
   p_move = moves_map[p_car_id][p_moves_avancement[p_car_id]]
   p_moves_avancement[p_car_id] += 1
   p_M = Update_Map(p_car_data)
   p_ok := p_car_data[p_car_id].check_and_move(p_move, p_M)
   if p_ok != true {
      // Something is preventing the car from moving
      p_move_max := 0
      if p_move >= 0 {
         _, p_move_max = IntSlice_MinMax(p_car_data[p_car_id].possible_moves(p_M))
      } else {
         p_move_max, _ = IntSlice_MinMax(p_car_data[p_car_id].possible_moves(p_M))
      }
      fmt.Println(green("Check, ", p_car_id, "::", p_move, " not ok. Max move: ", p_move_max))
      //
      rect_map, rect_order := moves_map, moves_order
      //
      fmt.Println(moves_map, moves_order)
      fmt.Println(p_car_id, p_move, p_moves_avancement[p_car_id])
      fmt.Scanf("%s")
      if p_car_id == 99 {
         fmt.Println(green("Applying max possible move to car 99..."))
         p_car_data[p_car_id].check_and_move(p_move_max, p_M)
         p_move -= p_move_max
      }
      rect_map, rect_order, p_car_data = Required_moves(p_car_id, p_move, p_M, p_car_data)
      p_M = Update_Map(p_car_data)
      // Remove the current last value, that will be duplicated...
      fmt.Println("Removing the last move order to avoid duplicates...")
      if len(moves_order) > 0 {
         moves_order = moves_order[:len(moves_order)-1]
      }
      if len(moves_map[p_car_id]) > 0 {
         moves_map[p_car_id] = moves_map[p_car_id][:len(moves_map[p_car_id])-1]
      }
      for _, v := range rect_order {
         moves_order = append(moves_order, v)
      }
      for k, v := range rect_map {
         for _, z := range v {
            moves_map[k] = append(moves_map[k], z)
         }
      }
      fmt.Println(moves_map, moves_order)
   } else {
      // temporary
      fmt.Println("The car could move as expected without further requirements...")
   }

   /*for _, p_car_id := range moves_order {
      var p_move int
      p_move = moves_map[p_car_id][p_moves_avancement[p_car_id]]
      p_moves_avancement[p_car_id] += 1
      //
      p_ok := p_car_data[p_car_id].check_and_move(p_move, p_M)
      p_M = Update_Map(p_car_data)
      if p_ok != true {
         fmt.Println("Check,", p_car_id, ":", p_move, " not ok")
         // New moves need to be inserted from here
         rect_map, rect_order := moves_map, moves_order
         fmt.Println(p_car_id, p_move, p_moves_avancement[p_car_id])
         fmt.Scanf("%s")
         rect_map, rect_order, p_car_data = Required_moves(p_car_id, p_move, p_M, p_car_data)
         // Remove the current last value, that will be duplicated...
         if len(moves_order) > 0 {
            moves_order = moves_order[:len(moves_order)-1]
         }
         if len(moves_map[p_car_id]) > 0 {
            moves_map[p_car_id] = moves_map[p_car_id][:len(moves_map[p_car_id])-1]
         }
         for _, v := range rect_order {
            moves_order = append(moves_order, v)
         }
         for k, v := range rect_map {
            for _, z := range v {
               moves_map[k] = append(moves_map[k], z)
            }
         }
         fmt.Println(moves_map, moves_order)
         break
      }
      //
      fmt.Println("Check,", p_car_id, ":", p_move, " ok")

   } // end of re-check and rectifications*/
   fmt.Println(yellow(fmt.Sprintf("End of check...")))
   fmt.Println(moves_map, moves_order)
   p_M = Update_Map(p_car_data)
   Display(p_M)
   //fmt.Println(p_car_data)
   return moves_map, moves_order, p_car_data
}

//Show the moves required to solve the puzzle
func Solve(M *[6][6]int, car_data map[int]Vehicle) {
   cyan := color.New(color.FgCyan).SprintFunc()
   var ok bool
   ok = false
   // Because the map isn't updated during the solving process,
   // it might be needed to go through several steps
   fmt.Println(cyan("\nSolving...\n"))
   moves_map, moves_order, car_data := Required_moves(99, 4, *M, car_data)
   //moves_map, moves_order, car_data = Required_moves(99, 1, *M, car_data)
   moves_avancement := map[int]int{}
   fmt.Println("\n")
   Display(*M)
   for _, car_id := range moves_order {
      var move int
      move = moves_map[car_id][moves_avancement[car_id]]
      moves_avancement[car_id] += 1
      ok = car_data[car_id].check_and_move(move, *M)
      if ok != true {
         break
      }
      *M = Update_Map(car_data)
      Display(*M)
      fmt.Println("Move car", car_id, "of", move)
      fmt.Scanf("%s")
   }
}

func main() {
   //yellow := color.New(color.FgYellow).SprintFunc()
   //cyan := color.New(color.FgCyan).SprintFunc()

   var M [6][6]int
   var car_data map[int]Vehicle
   /*
   var input_method int
   for (input_method != 1 && input_method != 2) {
      fmt.Println(cyan("Please select a method for the input of the current game state:"))
      fmt.Printf(cyan("   (1) Selective input: add vehicles one by one using their coordinates.\n"))
      fmt.Printf(cyan("   (2) Full input: fill the whole map, one coordinate at a time.\n"))
      fmt.Printf("Selection (1|2): ")
      _, err := fmt.Scanf("%d", &input_method)
      if err != nil {
            fmt.Println(err)
      }
      if (input_method != 1 && input_method != 2) {
         fmt.Println(yellow("Please choose either \"1\" or \"2\"."))
      }
      fmt.Printf("\n")
   }

	if input_method == 1 {
      car_data = Selective_Input(&M)
   } else if input_method == 2 {
      car_data = Full_Input(&M)
   }
   */
   car_data = map[int]Vehicle{
      1:Vehicle{1, "LR", 5, []int{0, 1}, "car"},
      2:Vehicle{2, "LR", 4, []int{0, 1}, "car"},
      3:Vehicle{3, "LR", 3, []int{0, 1}, "car"},
      4:Vehicle{4, "UD", 2, []int{2, 3}, "car"},
      5:Vehicle{5, "UD", 2, []int{4, 5}, "car"},
      6:Vehicle{6, "LR", 1, []int{2, 3}, "car"},
      7:Vehicle{7, "UD", 3, []int{2, 3}, "car"},
      8:Vehicle{8, "LR", 0, []int{3, 4}, "car"},
      9:Vehicle{9, "LR", 5, []int{3, 4, 5}, "truck"},
      10:Vehicle{10, "LR", 4, []int{3, 4, 5}, "truck"},
      11:Vehicle{11, "LR", 3, []int{4, 5}, "car"},
      12:Vehicle{12, "UD", 4, []int{1, 2}, "car"},
      13:Vehicle{13, "UD", 5, []int{0, 1, 2}, "truck"},
      99:Vehicle{99, "LR", 2, []int{0, 1}, "car"},
   }
   M = Update_Map(car_data)

   Display(M)
   fmt.Println(len(car_data), "car(s):", car_data)

   /*
   car_data[99].check_and_move(3, M)
   M = Update_Map(car_data)
   Display(M)
   */

   Solve(&M, car_data)
}


/*
Id int
Orientation Axis
Boundary_Row_or_Column int
Position [2]int
*/
